import os
import re
os.chdir('/Users/nuriaherreracepero/Downloads/benchmark/')
with open('benchmark.xml', 'w', encoding='utf8') as target:
	target.write("<?xml version='1.0' encoding='UTF-8'?>\n")
	target.write("<benchmark_root>\n")
	for folder in os.listdir():
		if re.match("\dtriples",folder):
			for file in os.listdir(folder):
				if "xml" in file:
					if "parsed" not in file:
						with open(folder + "/" + file, "r", encoding="utf8") as f:
							for num, line in enumerate(f):
								if num > 0:
									target.write(line)

			print(folder)
	target.write("</benchmark_root>")