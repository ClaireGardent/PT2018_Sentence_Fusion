#!/bin/sh
python3 translate.py -gpu 0 -model models/full/fullmodel_grele_100e_*_e3.pt -src data/baseline-seq2seq/training_pred/train.simple -tgt data/baseline-seq2seq/training_pred/train.complex -report_bleu -output predictions/81k_training_prediction_3e
python3 translate.py -gpu 0 -model models/full/fullmodel_grele_100e_*_e7.pt -src data/baseline-seq2seq/training_pred/train.simple -tgt data/baseline-seq2seq/training_pred/train.complex -report_bleu -output predictions/81k_training_prediction_7e
python3 translate.py -gpu 0 -model models/full/fullmodel_grele_100e_*_e15.pt -src data/baseline-seq2seq/training_pred/train.simple -tgt data/baseline-seq2seq/training_pred/train.complex -report_bleu -output predictions/81k_training_prediction_15e
python3 translate.py -gpu 0 -model models/full/fullmodel_grele_100e_*_e17.pt -src data/baseline-seq2seq/training_pred/train.simple -tgt data/baseline-seq2seq/training_pred/train.complex -report_bleu -output predictions/81k_training_prediction_17e
python3 translate.py -gpu 0 -model models/full/fullmodel_grele_100e_*_e13.pt -src data/baseline-seq2seq/training_pred/train.simple -tgt data/baseline-seq2seq/training_pred/train.complex -report_bleu -output predictions/81k_training_prediction_13e
python3 translate.py -gpu 0 -model models/full/fullmodel_grele_100e_*_e20.pt -src data/baseline-seq2seq/training_pred/train.simple -tgt data/baseline-seq2seq/training_pred/train.complex -report_bleu -output predictions/81k_training_prediction_20e
python3 translate.py -gpu 0 -model models/full/fullmodel_grele_100e_*_e23.pt -src data/baseline-seq2seq/training_pred/train.simple -tgt data/baseline-seq2seq/training_pred/train.complex -report_bleu -output predictions/81k_training_prediction_23e

