#!/bin/sh
python3 train.py -data data/baseline-seq2seq/full-grele/fulldata -save_model models/full/fullmodel_grele_20e -gpuid 0 -epochs 20
python3 translate.py -gpu 0 -model models/full/fullmodel_grele_20e*_e20.pt -src data/baseline-seq2seq/full/test.simple -output predictions/full_prediction_20e_0518