#!/bin/sh
python3 train.py -data data/baseline-seq2seq/full-grele/fulldata -save_model models/full/fullmodel_grele_100e -gpuid 0 -epochs 100
python3 translate.py -gpu 0 -model models/full/fullmodel_grele_100e*_e100.pt -src data/baseline-seq2seq/full/test.simple -output predictions/full_prediction_100e_0519