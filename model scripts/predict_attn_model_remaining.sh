#!/bin/sh
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e17.pt -src data/baseline-seq2seq/full/test.simple -output predictions/full_prediction_copy_attn_17e_cont
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e20.pt -src data/baseline-seq2seq/full/test.simple -output predictions/full_prediction_copy_attn_20e_cont
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e23.pt -src data/baseline-seq2seq/full/test.simple -output predictions/full_prediction_copy_attn_23e_cont
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e3.pt -src data/baseline-seq2seq/training_pred/train.simple -output predictions/81k_training_prediction_copy_attn_3e
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e7.pt -src data/baseline-seq2seq/training_pred/train.simple -output predictions/81k_training_prediction_copy_attn_7e
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e13.pt -src data/baseline-seq2seq/training_pred/train.simple -output predictions/81k_training_prediction_copy_attn_13e
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e15.pt -src data/baseline-seq2seq/training_pred/train.simple -output predictions/81k_training_prediction_copy_attn_15e
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e17.pt -src data/baseline-seq2seq/training_pred/train.simple -output predictions/81k_training_prediction_copy_attn_17e
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e20.pt -src data/baseline-seq2seq/training_pred/train.simple -output predictions/81k_training_prediction_copy_attn_20e
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e23.pt -src data/baseline-seq2seq/training_pred/train.simple -output predictions/81k_training_prediction_copy_attn_23e

