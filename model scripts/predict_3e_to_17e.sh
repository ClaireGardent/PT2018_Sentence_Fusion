#!/bin/sh
python3 translate.py -gpu 0 -model models/full/fullmodel_grele_100e_*_e3.pt -src data/baseline-seq2seq/full/test.simple -tgt data/baseline-seq2seq/full/test.complex -report_bleu -output predictions/full_prediction_3e_grele_3nodes
python3 translate.py -gpu 0 -model models/full/fullmodel_grele_100e_*_e7.pt -src data/baseline-seq2seq/full/test.simple -tgt data/baseline-seq2seq/full/test.complex -report_bleu -output predictions/full_prediction_7e_grele_3nodes
python3 translate.py -gpu 0 -model models/full/fullmodel_grele_100e_*_e15.pt -src data/baseline-seq2seq/full/test.simple -tgt data/baseline-seq2seq/full/test.complex -report_bleu -output predictions/full_prediction_15e_grele_3nodes
python3 translate.py -gpu 0 -model models/full/fullmodel_grele_100e_*_e17.pt -src data/baseline-seq2seq/full/test.simple -tgt data/baseline-seq2seq/full/test.complex -report_bleu -output predictions/full_prediction_17e_grele_3nodes
