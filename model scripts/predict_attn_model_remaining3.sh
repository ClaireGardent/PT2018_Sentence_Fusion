#!/bin/sh
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e17.pt -src data/baseline-seq2seq/training_pred/train.simple -output predictions/81k_training_prediction_copy_attn_17e_full
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e20.pt -src data/baseline-seq2seq/training_pred/train.simple -output predictions/81k_training_prediction_copy_attn_20e
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e23.pt -src data/baseline-seq2seq/training_pred/train.simple -output predictions/81k_training_prediction_copy_attn_23e

