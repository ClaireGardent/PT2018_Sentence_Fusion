#!/bin/sh
#python3 preprocess.py -train_src data/baseline-seq2seq/full/train.simple -train_tgt data/baseline-seq2seq/full/train.complex -valid_src data/baseline-seq2seq/full/validation.simple -valid_tgt data/baseline-seq2seq/full/validation.complex -src_seq_length 70 -tgt_seq_length 70 -save_data data/baseline-seq2seq/full/fulldata_dynamic_dict -dynamic_dict
python3 train.py -data data/baseline-seq2seq/full/fulldata_dynamic_dict -save_model models/full/fullmodel_copy_attn -gpuid 0 -epochs 23 -copy_attn
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e3.pt -src data/baseline-seq2seq/full/test.simple -output predictions/full_prediction_copy_attn_3e
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e7.pt -src data/baseline-seq2seq/full/test.simple -output predictions/full_prediction_copy_attn_7e
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e13.pt -src data/baseline-seq2seq/full/test.simple -output predictions/full_prediction_copy_attn_13e
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e15.pt -src data/baseline-seq2seq/full/test.simple -output predictions/full_prediction_copy_attn_15e
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e17.pt -src data/baseline-seq2seq/full/test.simple -output predictions/full_prediction_copy_attn_17e
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e20.pt -src data/baseline-seq2seq/full/test.simple -output predictions/full_prediction_copy_attn_20e
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e23.pt -src data/baseline-seq2seq/full/test.simple -output predictions/full_prediction_copy_attn_23e
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e3.pt -src data/baseline-seq2seq/training_pred/train.simple -output predictions/81k_training_prediction_copy_attn_3e
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e7.pt -src data/baseline-seq2seq/training_pred/train.simple -output predictions/81k_training_prediction_copy_attn_7e
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e13.pt -src data/baseline-seq2seq/training_pred/train.simple -output predictions/81k_training_prediction_copy_attn_13e
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e15.pt -src data/baseline-seq2seq/training_pred/train.simple -output predictions/81k_training_prediction_copy_attn_15e
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e17.pt -src data/baseline-seq2seq/training_pred/train.simple -output predictions/81k_training_prediction_copy_attn_17e
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e20.pt -src data/baseline-seq2seq/training_pred/train.simple -output predictions/81k_training_prediction_copy_attn_20e
python3 translate.py -gpu 0 -model models/full/fullmodel_copy_attn*_e23.pt -src data/baseline-seq2seq/training_pred/train.simple -output predictions/81k_training_prediction_copy_attn_23e