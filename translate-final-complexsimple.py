# -*- coding: utf-8 -*-
from googletrans import Translator
import re
import json
translator = Translator(service_urls=['translate.google.com','translate.google.co.kr',])

sourceFile = "benchmark/final-complexsimple-meanpreserve-intreeorder-full.txt"
targetFile = "benchmark/es-final-complexsimple-meanpreserve-intreeorder-full.txt"

def translate_en_es(sentence,num,trans_till_newline, trans_line):
    ''' 
    Translates english phrases to Spanish

    Arguments:
    English string

    Returns:
    Translation in string

    '''
     
    try:
         transObj = translator.translate(sentence, src='en', dest='es' )
    except Exception as e:
        print(str(num))
        print('**********'+'\n' + '  Run from after '+ num + ' last id ********** ,' + str(trans_till_newline) + ' , ' + str(trans_line))
        translate_file_to_file(sourceFile,targetFile,num,trans_till_newline, trans_line)

    #transObj = translator.translate("The architecture style of Adisham Hall is Tudor and Jacabian . Aidsham Hall is located in Sri Lanka . Adisham Hall was completed in 1931 .", src='en', dest='es' )

    return transObj.text
def append_ES(string):
    ''' Replaces ES with a newline character'''

    return string.replace("\n", "-ES\n")

def translate_file_to_file(source,destination,lastid=32255, trans_till_newlineA=False, trans_lineA=False):
    print('##########'+'\n' + 'Start from after '+ str(lastid)+ ' last id ########## ,' + str(trans_till_newlineA) + ' , ' + str(trans_lineA))
    ''' 
    Translates an specified english file to another specified file

    Arguments:
    source -> file to be translated
    destination ->file to write translation to
    Returns:
    doesn´t return anything, just makes the job

    '''
    with open(source, "r", encoding="utf-8") as f:    
        with open(destination, "a", encoding="utf-8") as transFile:
        
                
            #sentdata = []
            trans_till_newline = trans_till_newlineA
            trans_line = trans_lineA
            
            for num, line in enumerate(f, 1):
                if num > lastid:                    
                    print(str(num) + " " + str(lastid) + " " + str(counter))
                    if re.match('COMPLEX-[0-9]*\n', line):
                        trans_till_newline = True
                        print (line)
                        #transline = translate_en_es(line,num)
                        transFile.write(append_ES(line))
                        print(str(num) + " " + str(line.encode('utf8'))  )
                    elif re.match('COMPLEX-\d+:MR-\d+:SIMPLE-\d+\n', line):
                        trans_till_newline = True
                        transFile.write(append_ES(line))
                    elif trans_till_newline:
                        if not re.match('^\n', line):
                            transline = translate_en_es(line,num,trans_till_newline,trans_line)
                            transFile.write(transline + "\n")
                        else:
                            trans_till_newline = False
                            transFile.write(line)counter -= 1
                    elif re.match('COMPLEX-\d+:MR-\d+:SIMPLE-\d+:SPTYPE(-\d+){2,}:MR-\d+\n', line):
                        transFile.write(append_ES(line))counter -= 1
                    elif re.match('^category=', line):
                        trans_line = True
                        transFile.write(line)counter -= 1
                    elif trans_line:
                        transline = translate_en_es(line,num,trans_till_newline,trans_line)
                        transFile.write(transline + "\n")
                        trans_line = False
                    else:
                        transFile.write(line)
                    


    #for item in sentdata:
        	#transFile.write("%s\n")
#translate_file_to_file(source,destination,lastid=0):
translate_file_to_file(sourceFile,targetFile)
